package br.com.mastertech.fatura.service;

import br.com.mastertech.fatura.DTO.PagamentoDTO;
import br.com.mastertech.fatura.model.Cartao;
import br.com.mastertech.fatura.model.Fatura;
import br.com.mastertech.fatura.model.Status;
import br.com.mastertech.fatura.payment.CartaoFatura;
import br.com.mastertech.fatura.payment.ClientCardNotFoundException;
import br.com.mastertech.fatura.payment.PagamentoFatura;
import br.com.mastertech.fatura.payment.PaymentNotFoudException;
import br.com.mastertech.fatura.repository.FaturaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.time.LocalDate;
import java.util.List;

@Service
public class FaturaService {

    @Autowired
    private FaturaRepository faturaRepository;

    @Autowired
    private PagamentoFatura pagamentoFatura;

    @Autowired
    private CartaoFatura cartaoFatura;

    public Fatura pagarFaturaDoCartao(int clienteId, int cartaoId) {
        Fatura fatura;

        List<PagamentoDTO> pagamentosDTO = buscarFaturaDoClientePorCartao(clienteId, cartaoId);

        if (!pagamentosDTO.isEmpty()) {
            fatura = retornaReciboFatura(pagamentosDTO);

            pagamentoFatura.deletarPagamentoPeloIdCartao(cartaoId);

            return salvarFatura(fatura);
        } else {
            throw new PaymentNotFoudException();
        }
    }

    public List<PagamentoDTO> buscarFaturaDoClientePorCartao(int clienteId, int cartaoId) {
        Cartao cartao = cartaoFatura.buscarCartaoPorID(cartaoId);
        if (cartao.getCliente().getId() == clienteId) {
            List<PagamentoDTO> pagamentosDTO = pagamentoFatura.buscarPagamentoPorId(cartaoId);
            return pagamentosDTO;
        } else {
            throw new ClientCardNotFoundException();
        }
    }

    public Fatura retornaReciboFatura(List<PagamentoDTO> pagamentosDTO) {
        Fatura faturaPaga = new Fatura();
        LocalDate date = LocalDate.now();
        double valorPago = 0;
        for (PagamentoDTO pagamentoDTO : pagamentosDTO) {
            valorPago += pagamentoDTO.getValor();
        }
        faturaPaga.setValorPago(valorPago);
        faturaPaga.setPagoEm(date);

        return faturaPaga;
    }

    public Fatura salvarFatura(Fatura fatura) {
        return faturaRepository.save(fatura);
    }

    public Status inativarCartao(int clienteId, int cartaoId) {
        Cartao cartao = cartaoFatura.buscarCartaoPorID(cartaoId);
        if (cartao.getCliente().getId() == clienteId) {
            return cartaoFatura.inativarCartao(cartaoId);
        } else {
            throw new ClientCardNotFoundException();
        }
    }
}
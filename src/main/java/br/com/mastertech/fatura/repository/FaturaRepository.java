package br.com.mastertech.fatura.repository;

import br.com.mastertech.fatura.model.Fatura;
import org.springframework.data.repository.CrudRepository;

public interface FaturaRepository extends CrudRepository<Fatura, Integer> {
}

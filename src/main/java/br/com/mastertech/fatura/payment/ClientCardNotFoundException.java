package br.com.mastertech.fatura.payment;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Combinação id cliente e id cartão não encontrada ")
public class ClientCardNotFoundException extends RuntimeException{
}

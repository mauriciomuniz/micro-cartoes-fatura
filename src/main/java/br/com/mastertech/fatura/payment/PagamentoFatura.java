package br.com.mastertech.fatura.payment;

import br.com.mastertech.fatura.DTO.PagamentoDTO;
import br.com.mastertech.fatura.model.Pagamento;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@FeignClient(name = "PAGAMENTO")
public interface PagamentoFatura {

    @GetMapping("/pagamentos/{id_cartao}")
    List<PagamentoDTO> buscarPagamentoPorId(@PathVariable(name = "id_cartao") int idCartao);

    @DeleteMapping("/{id_cartao}")
    void deletarPagamentoPeloIdCartao(@PathVariable(name = "id_cartao") int idCartao);
}

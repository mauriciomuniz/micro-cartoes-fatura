package br.com.mastertech.fatura.payment;

import br.com.mastertech.fatura.model.Cartao;
import br.com.mastertech.fatura.model.Status;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "CARTAO")
public interface CartaoFatura {

    @GetMapping("/cartao/id/{id_cartao}")
    Cartao buscarCartaoPorID(@PathVariable(name = "id_cartao") int idCartao);

    @PatchMapping("/cartao/{cartao_id}/expirar")
    Status inativarCartao (@PathVariable(name = "cartao_id") int idCartao);
}

package br.com.mastertech.fatura.payment;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Não existem pagamentos a serem realizados")
public class PaymentNotFoudException extends RuntimeException{
}

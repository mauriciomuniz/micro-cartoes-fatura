package br.com.mastertech.fatura.controller;

import br.com.mastertech.fatura.DTO.PagamentoDTO;
import br.com.mastertech.fatura.model.Fatura;
import br.com.mastertech.fatura.model.Status;
import br.com.mastertech.fatura.service.FaturaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/fatura")
public class FaturaController {


    @Autowired
    private FaturaService faturaService;

    @PostMapping("/{cliente_id}/{cartao_id}/pagar")
    @ResponseStatus(HttpStatus.CREATED)
    public Fatura pagarFaturaDoCartao(@PathVariable(name = "cliente_id") int clienteId, @PathVariable(name = "cartao_id") int cartaoId) {
        Fatura faturaPaga = faturaService.pagarFaturaDoCartao(clienteId, cartaoId);
        return faturaPaga;
    }

    @GetMapping("/{cliente_id}/{cartao_id}")
    @ResponseStatus(HttpStatus.OK)
    public List<PagamentoDTO> buscarFaturaDoClientePorCartao(@PathVariable(name = "cliente_id") int clienteId, @PathVariable(name = "cartao_id") int cartaoId) {
        List<PagamentoDTO> fatura = faturaService.buscarFaturaDoClientePorCartao(clienteId, cartaoId);
        return fatura;
    }

    @PostMapping("/{cliente_id}/{cartao_id}/expirar")
    @ResponseStatus(HttpStatus.CREATED)
    public Status inativarCartao(@PathVariable(name = "cliente_id") int clienteId, @PathVariable(name = "cartao_id") int cartaoId) {
        Status status = faturaService.inativarCartao(clienteId, cartaoId);
        return status;
    }
}
